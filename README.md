Folder zawiera pliki potrzebne do obliczenia wartosci i wektorow wlasnych hamiltonianu dla studni kwantowej, dwiema metodami:
* metoda trojpunktowa
* metoda numerova

Pliki potrzebne do poprawnego wykoniania programu:
schrodinger.f90 -> glowny program napisany w jezyku fortran
m1.pg -> skrypt w gnuplocie wyrysowujacy wyniki dla metody trojpunktowej
m2.pg -> skrypt w gnuplocie wyrysowujacy wyniki dla metody numerova
schrodinger.sh -> skrypt w bashu przeprowadzajacy kompilacje i linkowanie z bibliotekami lapacka oraz uruchamiajacy skrypty w gnuplocie


Jako wynik dzialania programu otrzymywane sa nastepujace pliki:
metoda*.dat -> pliki zawierajace dane
metoda*.eps -> obrazy w eps z wyrysowanymi wykresami
