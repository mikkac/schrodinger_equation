	program schrodinger
	implicit none
	doubleprecision 	::well_width=10E-9,well_depth=1.0,m_reduced=0.1
	
	call metoda_trojpunktowa(well_width,well_depth,m_reduced)
	print *, "tri"
	well_depth=1.0
	call metoda_numerova(well_width,well_depth,m_reduced)
	
	end program

	subroutine metoda_trojpunktowa(well_width,well_depth,m_reduced)
	!implicit none
	integer,parameter	:: N=1000
	doubleprecision 	:: width,well_width,well_depth
	doubleprecision		:: m_reduced, m= 9.10938356E-31
	doubleprecision		:: q = 1.6021766208E-19, h = 1.054571800E-34
	doubleprecision 	:: dx, side, h2_2m = 0.0 !mnoznik operatorowy
	doubleprecision, dimension(N,N)	:: potential=0.0, kinetic=0.0
	doubleprecision, dimension(N,N)	:: hamiltonian = 0.0
	doubleprecision, dimension(N)	:: x=0.0
	integer				:: i = 0, j = 0	
	!lapack
	character	:: uplo = 'U', jobz = 'V' 
	integer		:: num = N, info = 0	
	doubleprecision, dimension(N*(N+1)/2)	:: hamiltonian_packed = 0.0
	doubleprecision, dimension(N)			:: energy_unordered = 0.0
	doubleprecision, dimension(N,N)			:: envelope_unordered = 0.0
	doubleprecision, dimension(3*N)			:: work = 0.0
	
	width=well_width*4	!wyznaczenie przedzialu, w ktorym wykres bedzie rysowany
	side = (width - well_width)*0.5	!wyznaczenie poczatku studni
	m = m_reduced*m
	h2_2m = h * h/ (2.0 * m)
	well_depth = well_depth * q 
	dx = width / N
	
	
	do i=1,N
		x(i)=i*dx
	end do
	
	!potencjałs
	do i=1,N
		if(i*dx <= side .OR. i*dx >= (side+well_width)) then !side- poczatek studni, side+well_width - koniec studni
			potential(i,i) = well_depth 	
		endif
	end do
	
	!energia kinetyczna
	do i=1,N
		kinetic(i,i) = -2.0
		if(i < N) then
			kinetic(i,i+1) = 1.0
		end if
		if(i > 1) then
			kinetic(i,i-1) = 1.0
		end if
	end do
	
	!hamiltonian
	do i=1,N
		do j=1,N
			hamiltonian(i,j) = potential(i,j) - h2_2m/(dx*dx)*kinetic(i,j)
		end do
	end do
	
	do j=1,N
		do i=1,N
			hamiltonian_packed(i+(j-1)*j/2) = hamiltonian(i,j)
		end do
	end do
	
	call dspev(jobz, uplo, num, hamiltonian_packed, energy_unordered, &
				envelope_unordered, num, work, info);
	if (info /= 0) then
		print *, "DSPEV wystąpił błąd \n"
		print *, "Info = ", info
	end if
	
	!wypisanie wynikow
	open(1, file='metoda_trojpunktowa.dat')
	do i=1,N
		write(1,201) x(i)*1e9, potential(i,i)/q, & !podzielenie wartosci potencjalu przez '10' jest tylko po to, aby lepiej go dopasowac na wykresie do reszty danych
		 envelope_unordered(i,1), &
		 envelope_unordered(i,2), &
		 envelope_unordered(i,3), &
		 energy_unordered(i)/q
	end do
	close(1)
201 format(' ',15E15.5)

	end subroutine

	subroutine metoda_numerova(well_width,well_depth,m_reduced)
	!implicit none
	integer,parameter	:: N=1000
	doubleprecision 	:: width,well_width,well_depth
	doubleprecision		:: m_reduced, m= 9.10938356E-31
	doubleprecision		:: q = 1.6021766208E-19, h = 1.054571800E-34
	doubleprecision 	:: dx, side, h2_2m = 0.0 !mnoznik operatorowy
	doubleprecision, dimension(N,N)	:: potential=0.0, kinetic=0.0
	doubleprecision, dimension(N,N)	:: factor=0.0, hamiltonian=0.0
	doubleprecision, dimension(N)	:: x=0.0
	integer				:: i = 0, j = 0	
	!lapack
	character	:: uplo = 'U', jobz = 'V' 
	integer		:: num = N, info = 0, itype = 1 
	doubleprecision, dimension(N*(N+1)/2)	:: hamiltonian_packed = 0.0
	doubleprecision, dimension(N*(N+1)/2)	:: factor_packed = 0.0	
	doubleprecision, dimension(N)			:: energy_unordered = 0.0
	doubleprecision, dimension(N,N)			:: envelope_unordered = 0.0
	doubleprecision, dimension(3*N)			:: work = 0.0
	
	width=well_width*4	!wyznaczenie przedzialu, w ktorym wykres bedzie rysowany
	side = (width - well_width)*0.5	!wyznaczenie poczatku studni
	m = m_reduced*m
	h2_2m = h * h/ (2.0 * m)
	well_depth = -well_depth * q 
	dx = width / N
	
	
	do i=1,N
		x(i)=i*dx
	end do
	
	!potencjał
	do i=1,N
		if(i*dx <= side .OR. i*dx >= (side+well_width)) then
			potential(i,i) = -5.0/6.0*well_depth
			if(i < N) then
			potential(i,i+1) = 1.0/12.0*well_depth
			end if
			if(i > 1) then
			kinetic(i,i-1) = 1.0/12.0*well_depth
			end if
		end if
	end do
	
	!energia kinetyczna
	do i=1,N
		kinetic(i,i) = -2.0
		if(i < N) then
			kinetic(i,i+1) = 1.0
		end if
		if(i > 1) then
			kinetic(i,i-1) = 1.0
		end if
	end do
	
	!hamiltonian
	do i=1,N
		do j=1,N
			hamiltonian(i,j) = potential(i,j) - h2_2m/(dx*dx)*kinetic(i,j)
		end do
	end do
	
	!macierz B (factor)
	do i=1,N
			factor(i,i) = 5.0/6.0
			if(i < N) then
			factor(i,i+1) = 1.0/12.0
			end if
			if(i > 1) then
			factor(i,i-1) = 1.0/12.0
			end if
	end do

	
	do j=1,N
		do i=1,N
			hamiltonian_packed(i+(j-1)*j/2) = hamiltonian(i,j)
		end do
	end do
	
	do j=1,N
		do i=1,N
			factor_packed(i+(j-1)*j/2) = factor(i,j)
		end do
	end do
	
	call dpptrf(uplo,num,factor_packed,info)		!faktoryzacja macierzy B(factor)- B=U**T*U
	
	if (info /= 0) then
		print *, "DPPTRF wystąpił błąd \n"
		print *, "Info = ", info
	end if
	
	call dspgst(itype,uplo,num,hamiltonian_packed,factor_packed,info)	!sprowadzenie hamiltonianu do postaci U*A*U**T
	
	if (info /= 0) then
		print *, "DSYGST wystąpił błąd \n"
		print *, "Info = ", info
	end if

	call dspev(jobz, uplo, num, hamiltonian_packed, energy_unordered, &
				envelope_unordered, num, work, info);
	if (info /= 0) then
		print *, "DSPEV wystąpił błąd \n"
		print *, "Info = ", info
	end if
	
	open(2, file='metoda_numerova.dat')


	do i=1,N
		write(2,201) x(i)*1e9, potential(i,i)/q, &
		 envelope_unordered(i,1), &
		 envelope_unordered(i,2), &
		 envelope_unordered(i,3), &
		 energy_unordered(i)/q
	end do
	
	close(2)
201 format(' ',15E15.5)

	end subroutine
