#!/bin/bash
rm *.exe *.o	#usuniecie smieci z poprzedniej kompilacji
gfortran -c schrodinger.f90	#kompilacja bez linkowania
gfortran *.o -L$HOME/libf77/$ARCH -llapack -lblas	#kompilacja +linkowanie bibliotek
mv a.out schrodinger.exe
./schrodinger.exe
gnuplot m1.pg	#wyrysowanie wynikow metoda trojpunktowa
gnuplot m2.pg	#wyrysowanie wynikow metoda numerova
